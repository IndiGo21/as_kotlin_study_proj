package com.example.as_kotlin_study_proj

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.perf.FirebasePerformance
import com.google.firebase.perf.metrics.AddTrace
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import kotlinx.android.synthetic.main.lab4_sdk.*
import kotlinx.android.synthetic.main.navigator.*


class MainActivity : AppCompatActivity()//,  NavigationView.OnNavigationItemSelectedListener

{
    private lateinit var appBarConfiguration: AppBarConfiguration
    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    private lateinit var mFirebasePerformance: FirebasePerformance

    @AddTrace(name = "onCreateTrace", enabled = true /* optional */)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setContentView(R.layout.navigator)
        setSupportActionBar(topAppBar)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_video_gui, R.id.nav_http, R.id.nav_camera, R.id.nav_oauth2, R.id.nav_vk, R.id.nav_cypher, R.id.nav_websocket,R.id.nav_JWT
            ), drawerLayout
        )
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        val myTrace = FirebasePerformance.getInstance().newTrace("test_trace")
        myTrace.start()
        navigationView.setupWithNavController(navController)
        setupActionBarWithNavController(navController, appBarConfiguration)
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "button")
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "btn")
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "hzsa")
        mFirebaseAnalytics!!.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, bundle)

        val bundle2 = Bundle()
        mFirebaseAnalytics!!.logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle2)
        mFirebaseAnalytics!!.logEvent("newEvent"){
            param("hey","hey")
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    //        val viewPager2 = findViewById<ViewPager2>(R.id.viewPager2)
//        val tabLayout = findViewById<TabLayout>(R.id.tabs)
//        val textView = findViewById<TextView>(R.id.title)
//        val adapter=FragmentAdapter(supportFragmentManager,lifecycle)
//        viewPager2.adapter = adapter
//
//
//
//        TabLayoutMediator(tabLayout,viewPager2){tab, position ->
//            when(position){
//                0->{
//                    tab.text="Lab1"
//                }
//                1->{
//                    tab.text="Lab2"
//                }
//                2->{
//                    tab.text="Lab3"
//                }
//                3->{
//                    tab.text="Lab4VkSdk"
//                }
//                4->{
//                    tab.text="Lab4Oauth2"
//                }
//                5->{
//                    tab.text="Lab5"
//                }
//                6->{
//                    tab.text="Lab6"
//                }
//            }
//        }.attach()
//        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
//            override fun onTabReselected(tab: TabLayout.Tab?) {
//            }
//
//            override fun onTabUnselected(tab: TabLayout.Tab?) {
//            }
//
//            override fun onTabSelected(tab: TabLayout.Tab?) {
//                when(tab?.position){
//                    0->{
//                        textView.setText(R.string.tab0)
//                    }
//                    1->{
//                        textView.setText(R.string.tab1)
//                    }
//                    2->{
//                        textView.setText(R.string.tab2)
//                    }
//                    3->{
//                        textView.setText(R.string.tab3)
//                    }
//                    4->{
//                        textView.setText(R.string.tab4)
//                    }
//                    5->{
//                        textView.setText(R.string.tab5)
//                    }
//
//                }
//            }
//
//        })
//
//    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val callback = object : VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                sdkToken.text = "token= " + token.accessToken
            }

            override fun onLoginFailed(errorCode: Int) {
            }
        }
        if (data == null || !VK.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}