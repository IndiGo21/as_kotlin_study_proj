package com.example.as_kotlin_study_proj

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.lab8.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.*
import java.util.*


class Lab8 : Fragment() {
    var token: String = "123"
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab8, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        no_authorization.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                sometext.text = withContext(Dispatchers.IO) {
                    val client = OkHttpClient()
                    val request = Request.Builder()
                        .url("https://jwt-study-app.herokuapp.com/unprotected")
                        .build()
                    val call: Call = client.newCall(request)
                    val response = call.execute()
                    var resp = response.body?.string()
                    resp = resp?.substring(resp.lastIndexOf(":") + 2, resp.lastIndexOf("}") - 1)
                        .toString()
                    resp
                }
            }

        }
        login.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                sometext.text = withContext(Dispatchers.IO) {
                    val client = OkHttpClient()
                    val formBody: RequestBody = FormBody.Builder()
                        .add("username", username.text.toString())
                        .add("password", password.text.toString())
                        .build()
                    val request = Request.Builder()
                        .url("https://jwt-study-app.herokuapp.com/login")
                        .post(formBody)
                        .build()
                    val call: Call = client.newCall(request)
                    val response = call.execute()
                    var resp = response.body?.string()
                    if (resp != "Could not verify!") {
                    token =
                        resp?.substring(resp!!.lastIndexOf(":") + 2, resp!!.lastIndexOf("}") - 1)
                            .toString()
                    token
                    }
                    else {
                        resp = "Wrong login or password"
                        resp
                    }
                }
            }

        }
        secret.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                var image1 = withContext(Dispatchers.IO) {
                    val client = OkHttpClient()
                    val url: HttpUrl = HttpUrl.Builder()
                        .scheme("https")
                        .host("jwt-study-app.herokuapp.com")
                        .addPathSegment("protected")
                        .addQueryParameter("token", token)
                        .build()
                    val request = Request.Builder()
                        .url(url)
                        .build()
                    val call: Call = client.newCall(request)
                    val response = call.execute()
                    var resp = response.body?.string()
                    var string1 =
                        resp?.substring(resp!!.lastIndexOf(":") + 2, resp!!.lastIndexOf("}") - 1)
                    if (string1 != "INVALID TOKEN") {

                        val imageBytes = Base64.getDecoder().decode(string1)
                        val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                        image
                    } else {
                        "Authenticate first"
                    }
                }
                    if(image1 is String) {
                        sometext.text = image1
                    }
                    else if (image1 is Bitmap){
                        secret_photo.setImageBitmap(image1)
                    }

            }

        }


    }
}
