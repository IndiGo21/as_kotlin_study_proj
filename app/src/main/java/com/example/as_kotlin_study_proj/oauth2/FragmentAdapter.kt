package com.example.as_kotlin_study_proj.oauth2

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.as_kotlin_study_proj.videoGUI.Lab1
import com.example.as_kotlin_study_proj.videoGUI.Lab2

class FragmentAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                Lab4SDK()
            }
            1 -> {
                Lab4Oauth2()
            }
            else -> {
                Lab4SDK()
            }
        }
    }
}