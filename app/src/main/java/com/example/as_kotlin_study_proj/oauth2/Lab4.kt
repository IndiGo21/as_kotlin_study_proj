package com.example.as_kotlin_study_proj.oauth2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.as_kotlin_study_proj.R
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.perf.metrics.AddTrace
import kotlinx.android.synthetic.main.lab4.*

class Lab4 : Fragment() {
    @AddTrace(name = "lab4", enabled = true /* optional */)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab4, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = FragmentAdapter(childFragmentManager, lifecycle)
        viewPager2_vk.adapter = adapter

        TabLayoutMediator(tabs_vk, viewPager2_vk) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = "SDK"
                }
                1 -> {
                    tab.text = "Oauth2"
                }
            }

        }.attach()


    }
}