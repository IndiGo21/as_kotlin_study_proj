package com.example.as_kotlin_study_proj.oauth2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.as_kotlin_study_proj.R
import com.google.firebase.perf.metrics.AddTrace
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKScope
//import com.vk.sdk.api.friends.dto.FriendsGetFieldsResponse
import kotlinx.android.synthetic.main.lab4.*
import kotlinx.android.synthetic.main.lab4_sdk.*


class Lab4SDK : Fragment() {
    @AddTrace(name = "lab4SDK", enabled = true /* optional */)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab4_sdk, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sdkGetToken.setOnClickListener {
            VK.login(
                activity as FragmentActivity, arrayListOf(
                    VKScope.WALL,
                    VKScope.PHOTOS,
                    VKScope.FRIENDS,
                    VKScope.EMAIL,
                    VKScope.DOCS,
                    VKScope.GROUPS,
                    VKScope.PAGES,
                    VKScope.OFFLINE
                )
            )
        }
    }


}