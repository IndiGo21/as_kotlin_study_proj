package com.example.as_kotlin_study_proj.oauth2

import android.annotation.TargetApi
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.example.as_kotlin_study_proj.R
import kotlinx.android.synthetic.main.lab4_oauth2.*

private class MyWebViewClient : WebViewClient() {
    @TargetApi(Build.VERSION_CODES.N)
    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
        if (request.url.toString().startsWith("https://oauth.vk.com/blank.html")) {
            view.loadUrl(request.url.toString())
//            view.stopLoading()
            return true

        }
        view.loadUrl(request.url.toString())
        return true
    }
}

class Lab4Oauth2 : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab4_oauth2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val redirectUri = "https://oauth.vk.com/blank.html"
        val appId = "7831084"
        //       val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://oauth.vk.com/authorize?client_id=$appId&display=page&redirect_uri=$redirectUri&scope=friends&response_type=token&v=5.52"))
        openOauthAutorization.setOnClickListener {
//            startActivity(intent)
            oauthWebView.webViewClient = MyWebViewClient()
            // включаем поддержку JavaScript
            oauthWebView.settings.javaScriptEnabled = true
            // указываем страницу загрузки
            oauthWebView.loadUrl("https://oauth.vk.com/authorize?client_id=$appId&display=page&redirect_uri=$redirectUri&scope=friends&response_type=token&v=5.52")
        }
        showOauthToken.setOnClickListener {
            if (oauthWebView.url.toString().startsWith(redirectUri)) {
                oauthToken.text =
                    "token= " + oauthWebView.url.toString().substringAfter("=").substringBefore("&")
            } else {
                oauthToken.text = "Firstly authenticate"
            }


        }
    }
}