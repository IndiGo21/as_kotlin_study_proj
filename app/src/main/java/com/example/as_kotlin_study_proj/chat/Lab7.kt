package com.example.as_kotlin_study_proj.chat

import android.media.midi.MidiManager
import android.os.Bundle
import android.os.Message
import android.os.Parcelable
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.as_kotlin_study_proj.R
import com.google.firebase.perf.metrics.AddTrace
import kotlinx.android.synthetic.main.lab7.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.*
import okhttp3.internal.ws.WebSocketReader
import okio.ByteString
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class Lab7 : Fragment() {
    private val FILENAME_FORMAT_DATE = "MM-dd"
    private val FILENAME_FORMAT_TIME = "HH-mm-ss"
    private var mMessageRecycler: RecyclerView? = null
    private var mMessageAdapter: MessageListAdapter? = null
    private var wssuri = "wss://websocket-python-1.herokuapp.com/echo"
    lateinit var messageList: MutableList<BaseMessage>

    @AddTrace(name = "lab7", enabled = true /* optional */)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab7, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var messageList: MutableList<BaseMessage> = arrayListOf()
        mMessageRecycler = recycler_gchat as RecyclerView
        mMessageAdapter = MessageListAdapter(messageList)
        mMessageRecycler!!.layoutManager =  LinearLayoutManager(activity)
        mMessageRecycler!!.adapter = mMessageAdapter

        class MyWebSocketListener : WebSocketListener() {
            override fun onOpen(webSocket: WebSocket, response: Response) {
                super.onOpen(webSocket, response)
            }

            override fun onMessage(webSocket: WebSocket, text: String) {
                super.onMessage(webSocket, text)
                GlobalScope.launch(Dispatchers.Main) {
                    messageList.add(BaseMessage(text, 2, SimpleDateFormat(
                        FILENAME_FORMAT_DATE, Locale.US
                    ).format(System.currentTimeMillis()), SimpleDateFormat(
                        FILENAME_FORMAT_TIME, Locale.US
                    ).format(System.currentTimeMillis())))
                    (mMessageRecycler!!.adapter as MessageListAdapter).notifyDataSetChanged()
                }

            }

        }
        val client = OkHttpClient()
        val request: Request = Request.Builder().url(wssuri).build()
        val listener = MyWebSocketListener()
        val ws: WebSocket = client.newWebSocket(request,listener)
        button_gchat_send.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                var soc_msg = edit_gchat_message.text
                messageList.add(BaseMessage(soc_msg.toString(), 1, SimpleDateFormat(
                    FILENAME_FORMAT_DATE, Locale.US
                ).format(System.currentTimeMillis()), SimpleDateFormat(
                    FILENAME_FORMAT_TIME, Locale.US
                ).format(System.currentTimeMillis())))
                ws.send((soc_msg.toString()))
                (mMessageRecycler!!.adapter as MessageListAdapter).notifyDataSetChanged()
                edit_gchat_message.text.clear()
            }
        }
    }



}