package com.example.as_kotlin_study_proj.chat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.as_kotlin_study_proj.R


class MessageListAdapter(private val messageList: List<BaseMessage>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class SentMessageHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView? = null
        var timeText: TextView? = null
        var dateText: TextView? = null

        init {
            messageText = itemView.findViewById(R.id.text_gchat_message_me)
            dateText = itemView.findViewById(R.id.text_gchat_date_me)
            timeText = itemView.findViewById(R.id.text_gchat_timestamp_me)
        }
    }

    class ReceivedMessageHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView? = null
        var timeText: TextView? = null
        var dateText: TextView? = null

        init {
            messageText = itemView.findViewById(R.id.text_gchat_message_other)
            timeText = itemView.findViewById(R.id.text_gchat_timestamp_other)
            dateText = itemView.findViewById(R.id.text_gchat_date_other)
        }
    }

    // Determines the appropriate ViewType according to the sender of the message.
    override fun getItemViewType(position: Int): Int {
        return if (messageList[position].id == 1) {
            // If the current user is the sender of the message
            VIEW_TYPE_MESSAGE_SENT
        } else {
            // If some other user sent the message
            VIEW_TYPE_MESSAGE_RECEIVED
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_chat_me, parent, false)
            SentMessageHolder(view)
        } else {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_chat_other, parent, false)
            ReceivedMessageHolder(view)
        }
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType){
            1 -> {
                val sentMessageHolder: SentMessageHolder = holder as SentMessageHolder
                if (position > 0 && messageList[position].date == messageList[position-1].date){
                    sentMessageHolder.dateText?.visibility = View.GONE
                }
                else{
                    sentMessageHolder.dateText?.text = messageList[position].date
                }
                sentMessageHolder.messageText?.text = messageList[position].text
                sentMessageHolder.timeText?.text = messageList[position].time
            }
            else -> {
                val receivedMessageHolder: ReceivedMessageHolder = holder as ReceivedMessageHolder
                if (position > 0 && messageList[position].date == messageList[position-1].date){
                    receivedMessageHolder.dateText?.visibility = View.GONE
                }
                else{
                    receivedMessageHolder.dateText?.text = messageList[position].date
                }
                receivedMessageHolder.messageText?.text = messageList[position].text
                receivedMessageHolder.dateText?.text = messageList[position].date
                receivedMessageHolder.timeText?.text = messageList[position].time

            }
        }
    }


    companion object {
        private const val VIEW_TYPE_MESSAGE_SENT = 1
        private const val VIEW_TYPE_MESSAGE_RECEIVED = 2
    }


    override fun getItemCount(): Int {
        return messageList.size
    }

}