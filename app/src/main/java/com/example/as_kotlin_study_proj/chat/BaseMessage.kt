package com.example.as_kotlin_study_proj.chat

class BaseMessage(
    val text: String?,
    val id: Int?,
    val date: String?,
    val time:String?,
) {
}