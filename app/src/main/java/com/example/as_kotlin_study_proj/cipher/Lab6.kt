package com.example.as_kotlin_study_proj.cipher

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.as_kotlin_study_proj.R
import kotlinx.android.synthetic.main.lab6.*
import java.io.*
import java.security.MessageDigest
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.spec.SecretKeySpec


class Lab6 : Fragment() {

    var inputPath: Uri = Uri.parse("asdf")
    private val REQUEST_ID_PERMISSION = 200
    private lateinit var pass: ByteArray
    lateinit var decFile: File
    lateinit var readfis: FileInputStream

    // File created you can do anything
    val createFileEn = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
                        result.data?.data?.let {
                            encryptBuf(pass,inputPath,it)
            }

        }
    }

//    val createFileDe = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
//        if (result.resultCode == Activity.RESULT_OK) {
//            result.data?.data?.let {
//                decryptNew(pass,inputPath)
//            }
//
//        }
//    }
//private fun getOutputDirectory(): File? {
//    val mediaDir = context?.externalMediaDirs?.firstOrNull()?.let {
//        File(it, resources.getString(R.string.app_name)).apply { mkdirs() }
//    }
//    return if (mediaDir != null && mediaDir.exists())
//        mediaDir else context?.filesDir
//}
//    private fun writeToFile(uri: Uri, text: ByteArray) {
//        try {
//            context?.contentResolver?.openFileDescriptor(uri, "w")?.use { parcelFileDescriptor ->
//                FileOutputStream(parcelFileDescriptor.fileDescriptor).use {
//                    it.write(text)
//                }
//            }
//        } catch (e: FileNotFoundException) {
//            e.printStackTrace()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab6, container, false)
    }

    fun createFileEn() {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "text/plain"
        }
        createFileEn.launch(intent)
    }
//    fun createFileDe() {
//        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
//            addCategory(Intent.CATEGORY_OPENABLE)
//            type = "text/plain"
//        }
//        createFileDe.launch(intent)
//    }
    private fun checkPermissions(): Boolean {
        return ((ActivityCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.READ_EXTERNAL_STORAGE
        )) == PackageManager.PERMISSION_GRANTED
                && (ActivityCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )) == PackageManager.PERMISSION_GRANTED)
    }


    private fun encryptBuf(pass: ByteArray, filePathIn: Uri, filePathOut: Uri) {
        val sks = SecretKeySpec(
            pass,
            "AES"
        )
        val fis = context?.contentResolver?.openInputStream(filePathIn)
        val fos = context?.contentResolver?.openOutputStream(filePathOut)
        val cipher: Cipher = Cipher.getInstance("AES")
        cipher.init(Cipher.ENCRYPT_MODE, sks)
        val buffer = ByteArray(128)
        var read: Int
        try {
            while (((fis?.read(buffer) ?: 0).also { read = it }) > 0) {
                if (read < buffer.size) {
                    val cipherText = cipher.doFinal(buffer, 0, read)
                    fos?.write(cipherText)
                }
                else{
                    val cipherText = cipher.update(buffer, 0, read)
                    fos?.write(cipherText)
                }

            }
        }catch (e: Exception){
            textCrypt.text = e.toString()
        }
        fis?.close()
        fos?.close()
    }
    fun decryptBuf(pass: ByteArray, filePathIn: Uri): File {
        val sks = SecretKeySpec(
            pass,
            "AES"
        )
        val fis = context?.contentResolver?.openInputStream(filePathIn)
        val file = File(requireContext().filesDir.absolutePath + "/txt.txt")
        val fos = FileOutputStream(file, true)
        val cipher: Cipher = Cipher.getInstance("AES")
        cipher.init(Cipher.DECRYPT_MODE, sks)
        val buffer = ByteArray(128)
        var read: Int
        try {
            while (((fis?.read(buffer) ?: 0).also { read = it }) > 0) {
                if (read < buffer.size) {
                    val cipherText = cipher.doFinal(buffer, 0, read)
                    fos.write(cipherText)
                }
                else{
                    val cipherText = cipher.update(buffer, 0, read)
                    fos.write(cipherText)
                }

            }
        }catch (e: Exception){
            textCrypt.text = e.toString()
        }
        fis?.close()
        fos.close()
        return file
    }

private fun encrypt(pass: ByteArray, filePath: Uri ): ByteArray {
        // Here you read the cleartext.
        val stringBuilder = StringBuilder()
        context?.contentResolver?.openInputStream(filePath)?.use { inputStream ->
            BufferedReader(InputStreamReader(inputStream)).use { reader ->
                var line: String? = reader.readLine()
                while (line != null) {
                    stringBuilder.append(line)
                    line = reader.readLine()
                }
            }
        }
//        val extStore = getOutputDirectory()
//        val fis = FileInputStream(filePath) //"$extStore/lol.txt")
        // This stream write the encrypted text. This stream will be wrapped by
        // another stream.
//        val fos = FileOutputStream("$extStore/encrypted")

        // Length is 16 byte
        val sks = SecretKeySpec(
            pass,
            "AES"
        )
        val iv = ByteArray(16)
        SecureRandom().nextBytes(iv)
        // Create cipher
        val cipher = Cipher.getInstance("AES")
        cipher.init(Cipher.ENCRYPT_MODE, sks)
        val g = cipher.doFinal(stringBuilder.toString().toByteArray())
        return g
        // Wrap the output stream
//        val cos = CipherOutputStream(fos, cipher)
//        // Write bytes
//        var b: Int
//        val d = ByteArray(8)
//        while (fis.read(d).also { b = it } != -1) {
//            cos.write(d, 0, b)
//        }
//        // Flush and close streams.
//        cos.flush()
//        cos.close()
//        fis.close()
    }

private fun decrypt(pass: ByteArray, filePath: Uri):String {

        val stringBuilder = StringBuilder()
    try {

        context?.contentResolver?.openInputStream(filePath)?.use { inputStream ->
//            BufferedReader(InputStreamReader(inputStream)).use { reader ->
//                var line: String? = reader.readLine()
//                while (line != null) {
//                    stringBuilder.append(line)
//                    line = reader.readLine()
//                }
//            }
//        }
            val sks = SecretKeySpec(
                pass,
                "AES"
            )
            val cipher = Cipher.getInstance("AES")
            cipher.init(Cipher.DECRYPT_MODE, sks)
//        val g = cipher.doFinal(stringBuilder.toString().toByteArray())
            val cis = CipherInputStream(inputStream, cipher)
            var b: Int
            var g: String = ""
            val d = ByteArray(8)
            while (cis.read(d).also { b = it } != -1) {
                g += String(d)
                //            fos.write(d, 0, b)
            }
            //        fos.flush()
            //        fos.close()
            cis.close()
            return g
        }
    }
    catch (e : Exception){
        return e.toString()
    }
    return "ads"
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        encrypting.setOnClickListener {
            activity?.requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_ID_PERMISSION)
            if (!checkPermissions()) {
                Toast.makeText(requireContext(),
                    "You do not allow this app to read and write files.", Toast.LENGTH_LONG).show()
            }
            else {
                val md: MessageDigest = MessageDigest.getInstance("SHA-256")
                md.update(key_input.text.toString().toByteArray(Charsets.UTF_8))
                pass = md.digest()
                createFileEn()
//                enc = encrypt(pass, inputPath)
//                textCrypt.text = String(enc)
//                GlobalScope.launch(Dispatchers.Main) {
//                    withContext(Dispatchers.IO) {
//                        encrypt(pass, inputPath)
//                    }
//                }
            }
        }
        choose_file.setOnClickListener {
            val chooseFile = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "text/plain"
            }

//            chooseFile = Intent.createChooser(chooseFile, "Choose a file")
            startForResult.launch(chooseFile)

            //activity?.startActivityForResult(chooseFile, 15001)

        }
        decrypting.setOnClickListener {
//                val buff = ByteArray(512)
//                var bytes: Int = inputStream.read(buff)
//                var str: String
//                while (bytes != -1) {
//                    bytes = inputStream.read(buff)
//                    str = buff.decodeToString()
//                    val replacementChar = Char(0)
//                    val indexOfRC = str.indexOf(replacementChar)
//                    str = if (indexOfRC == -1) str else str.substring(0, indexOfRC)
//                    stringBuilder.append(str)
//                }
            activity?.requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_ID_PERMISSION)
            if (!checkPermissions()) {
                Toast.makeText(requireContext(),
                    "You do not allow this app to read and write files.", Toast.LENGTH_LONG).show()
            }
            else {
                val md: MessageDigest = MessageDigest.getInstance("SHA-256")
                md.update(key_input.text.toString().toByteArray(Charsets.UTF_8))
                pass = md.digest()
                decFile = decryptBuf(pass, inputPath)
                showText.isEnabled = true
                readfis = FileInputStream(decFile)
//                GlobalScope.launch(Dispatchers.Main) {
//                    textCrypt.text = withContext(Dispatchers.IO) {
//                        decrypt(pass, inputPath)
//                    }
//                }
            }
        }

        showText.setOnClickListener {

            var hmm: ByteArray? = ByteArray(1024)
            readfis.read(hmm)
            textCrypt.text = hmm?.decodeToString() ?: "sd"
            hmm = null
        }


    }

    val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            result.data?.data?.let {
                inputPath = it
                val t = it.lastPathSegment.toString()
                choose_file_path.text = t
                encrypting.isEnabled = true
                decrypting.isEnabled = true
            }
            // Handle the Intent
        }
    }

}