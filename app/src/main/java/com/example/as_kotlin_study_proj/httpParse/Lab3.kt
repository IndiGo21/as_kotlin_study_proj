package com.example.as_kotlin_study_proj.httpParse

import android.os.Bundle
import android.os.StrictMode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.as_kotlin_study_proj.R
import com.google.firebase.perf.metrics.AddTrace
import kotlinx.android.synthetic.main.lab3.*
import org.jsoup.Jsoup


class Lab3 : Fragment() {

    @AddTrace(name = "lab3", enabled = true /* optional */)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()

        StrictMode.setThreadPolicy(policy)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


//        queue.add(stringRequest)
//        showContent.setOnClickListener{
//            val document: LiveData<Document> = liveData {
//                val data = Jsoup.connect("https://ru.investing.com/currencies/usd-rub").get()
//                val element = data.select("pid-2186-last")
//                textView.text = "Курс доллара ${data.getElementById("last_last").text()} ₽"
//                textViewHTML.text = data.html()
//            }
//            fun document() = LiveData<Document>
//            document.
//        }

        showContent.setOnClickListener {
            val queue = Volley.newRequestQueue(context)
            val url = "https://ru.investing.com/currencies/usd-rub"
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                Response.Listener<String> { response ->
                    var doc = Jsoup.parse(response)
                    val newsHeadlines: String =
                        "Курс доллара ${doc.getElementsByClass("pid-2186-last").text()} ₽"
                    println(newsHeadlines)
                    textView.text = newsHeadlines
//                    for (headline in newsHeadlines) {
//                        println(headline.text())
//                        textView.text = headline.text()
//                    }
                    textViewHTML.text = doc.body().html()
                },
                Response.ErrorListener { textView.text = "That didn't work!" })
            queue.add(stringRequest)
        }


//        showContent.setOnClickListener{suspend fun fetchTwoDocs() =
//            coroutineScope {
//                val document =
//                    async { Jsoup.connect("https://ru.investing.com/currencies/usd-rub").get() }
//                var nice = document.await()
//                println(nice.html())
//                textView.text = "Курс доллара ${nice.getElementById("last_last").text()} ₽"
//                textViewHTML.text = nice.html()
//            }
//
//        }
//
    }

}