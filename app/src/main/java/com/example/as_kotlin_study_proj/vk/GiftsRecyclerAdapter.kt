package com.example.as_kotlin_study_proj.vk

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.as_kotlin_study_proj.R
import com.vk.sdk.api.gifts.dto.GiftsGetResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.InputStream
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class GiftsRecyclerAdapter(private val gifts: GiftsGetResponse) :
    RecyclerView.Adapter<GiftsRecyclerAdapter.GiftsViewHolder>() {

    val sdf = SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

    class GiftsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var date: TextView? = null
        var imgview: ImageView? = null
        var message: TextView? = null

        init {
            message = itemView.findViewById(R.id.giftMessage)
            imgview = itemView.findViewById(R.id.giftImage)
            date = itemView.findViewById(R.id.giftDate)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiftsViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_gift, parent, false)
        return GiftsViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return gifts.items!!.size
    }

    override fun onBindViewHolder(holder: GiftsViewHolder, position: Int) {
        holder.message?.text = "message: " + gifts.items!![position].message
        holder.date?.text =
            "date: " + sdf.format(Date(gifts.items!![position].date!!.toLong() * 1000))
        holder.imgview?.setImageResource(R.drawable.ic_study_proj_foreground)
        GlobalScope.launch(Dispatchers.Main) {
            val drawable = withContext(Dispatchers.IO) {
                Drawable.createFromStream(
                    URL(gifts.items!![position].gift!!.thumb256).getContent() as InputStream,
                    "src name"
                )
            }
            holder.imgview?.setImageDrawable(drawable)
        }
    }

}