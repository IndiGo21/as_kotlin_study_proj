package com.example.as_kotlin_study_proj.vk

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.as_kotlin_study_proj.R
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiCallback
import com.vk.sdk.api.friends.FriendsService
import com.vk.sdk.api.friends.dto.FriendsGetFieldsResponse
import com.vk.sdk.api.gifts.GiftsService
import com.vk.sdk.api.gifts.dto.GiftsGetResponse
import com.vk.sdk.api.users.dto.UsersFields
import kotlinx.android.synthetic.main.lab5.*


class Lab5 : Fragment() {

    lateinit var friends: List<FriendModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab5, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView: RecyclerView = recyclerView
        recyclerView.layoutManager = LinearLayoutManager(activity)

        showGifts.setOnClickListener {
            VK.execute(GiftsService().giftsGet(userId = VK.getUserId()), object :
                VKApiCallback<GiftsGetResponse> {
                override fun success(result: GiftsGetResponse) {
                    recyclerView.adapter = GiftsRecyclerAdapter(result)
                }

                override fun fail(error: Exception) {
                }
            })
        }

        showFriends.setOnClickListener {
            val fields = listOf(UsersFields.PHOTO_200)
            VK.execute(FriendsService().friendsGet(fields = fields), object :
                VKApiCallback<FriendsGetFieldsResponse> {
                override fun success(result: FriendsGetFieldsResponse) {
                    friends = result.items.map { friend ->
                        FriendModel(
                            firstName = friend.firstName,
                            id = friend.id,
                            lastName = friend.lastName,
                            photo = friend.photo200
                        )
                    }
                    println(friends)
                    recyclerView.adapter = FriendsRecyclerAdapter(friends)
                }

                override fun fail(error: Exception) {
                }
            })
        }
    }

}
