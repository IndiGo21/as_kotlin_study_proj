package com.example.as_kotlin_study_proj.vk

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.as_kotlin_study_proj.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.InputStream
import java.net.URL


class FriendsRecyclerAdapter(private val friends: List<FriendModel>) :
    RecyclerView.Adapter<FriendsRecyclerAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView? = null
        var imgview: ImageView? = null
        var surname: TextView? = null

        init {
            name = itemView.findViewById(R.id.giftMessage)
            imgview = itemView.findViewById(R.id.giftImage)
            surname = itemView.findViewById(R.id.giftDate)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_friend, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.name?.text = "name: " + friends[position].firstName
        holder.surname?.text = "surname: " + friends[position].lastName
        holder.imgview?.setImageResource(R.drawable.ic_study_proj_foreground)
        GlobalScope.launch(Dispatchers.Main) {
            val drawable = withContext(Dispatchers.IO) {
                Drawable.createFromStream(
                    URL(friends[position].photo).getContent() as InputStream,
                    "src name"
                )
            }
            holder.imgview?.setImageDrawable(drawable)
        }
//        val inputStream: InputStream = URL(friends[position].photo).getContent() as InputStream
//        val drawable = Drawable.createFromStream(inputStream, "src name")
//        holder.imgview?.setImageDrawable(drawable)
    }

    override fun getItemCount(): Int {
        return friends.size
    }
}