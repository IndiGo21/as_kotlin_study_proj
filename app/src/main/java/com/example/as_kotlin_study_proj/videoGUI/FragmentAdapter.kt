package com.example.as_kotlin_study_proj.videoGUI

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.as_kotlin_study_proj.videoGUI.Lab1
import com.example.as_kotlin_study_proj.videoGUI.Lab2

class FragmentAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                Lab1()
            }
            1 -> {
                Lab2()
            }
            else -> {
                Lab1()
            }
        }
    }
}