package com.example.as_kotlin_study_proj.videoGUI

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.as_kotlin_study_proj.R
import com.google.firebase.perf.metrics.AddTrace
import kotlinx.android.synthetic.main.lab1.*


class Lab1 : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    @AddTrace(name = "lab1", enabled = true /* optional */)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab1, container, false)
    }

    @AddTrace(name = "Button clicked", enabled = true /* optional */)
    fun something(){
        print("fadsf")
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button.setOnClickListener {
            something()
        }

    }


}