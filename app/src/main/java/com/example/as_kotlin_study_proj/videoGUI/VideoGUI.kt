package com.example.as_kotlin_study_proj.videoGUI

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.as_kotlin_study_proj.R
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.video_gui.*

class VideoGUI : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.video_gui, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = FragmentAdapter(childFragmentManager, lifecycle)
        viewPager2_gui.adapter = adapter

        TabLayoutMediator(tabs_gui, viewPager2_gui) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = "GUI"
                }
                1 -> {
                    tab.text = "Video"
                }
            }

        }.attach()


    }
}