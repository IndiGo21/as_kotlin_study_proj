package com.example.as_kotlin_study_proj.videoGUI

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.fragment.app.Fragment
import com.example.as_kotlin_study_proj.R
import com.google.firebase.perf.metrics.AddTrace
import kotlinx.android.synthetic.main.lab2.*


class Lab2 : Fragment() {

    @AddTrace(name = "lab2", enabled = true /* optional */)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lab2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        videoView.setVideoURI(Uri.parse("http://techslides.com/demos/samples/sample.webm"))
        val mediaController = MediaController(activity)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
    }
}